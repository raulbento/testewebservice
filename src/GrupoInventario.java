import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement(name = "grupoInventario")
public class GrupoInventario {

	private Long id;
	private Date criacao;
	private Estabelecimento estabelecimento;
	private boolean completo;
	private List<Inventario> inventarios;
	private boolean registroAtivo;

	public GrupoInventario() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoInventario other = (GrupoInventario) obj;
		if (completo != other.completo)
			return false;
		if (criacao == null) {
			if (other.criacao != null)
				return false;
		} else if (!criacao.equals(other.criacao))
			return false;
		if (estabelecimento == null) {
			if (other.estabelecimento != null)
				return false;
		} else if (!estabelecimento.equals(other.estabelecimento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inventarios == null) {
			if (other.inventarios != null)
				return false;
		} else if (!inventarios.equals(other.inventarios))
			return false;
		if (registroAtivo != other.registroAtivo)
			return false;
		return true;
	}

	@JsonProperty("criacao")
	@XmlElement(name = "criacao")
	public Date getCriacao() {
		return criacao;
	}

	@JsonProperty("estabelecimento")
	@XmlElement(name = "estabelecimento")
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	@JsonProperty("id")
	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	@JsonProperty("inventarios")
	@XmlElement(name = "inventarios")
	public List<Inventario> getInventarios() {
		if (inventarios == null) {
			inventarios = new ArrayList<Inventario>();
		}
		return inventarios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (completo ? 1231 : 1237);
		result = prime * result + ((criacao == null) ? 0 : criacao.hashCode());
		result = prime * result + ((estabelecimento == null) ? 0 : estabelecimento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inventarios == null) ? 0 : inventarios.hashCode());
		result = prime * result + (registroAtivo ? 1231 : 1237);
		return result;
	}

	@JsonProperty("completo")
	@XmlElement(name = "completo")
	public boolean isCompleto() {
		return completo;
	}

	@JsonProperty("registroAtivo")
	@XmlElement(name = "registroAtivo")
	public boolean isRegistroAtivo() {
		return registroAtivo;
	}

	public void setCompleto(boolean completo) {
		this.completo = completo;
	}

	public void setCriacao(Date criacao) {
		this.criacao = criacao;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setInventarios(List<Inventario> inventarios) {
		this.inventarios = inventarios;
	}

	public void setRegistroAtivo(boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

}