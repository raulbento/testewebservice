import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement(name = "inventario")
public class Inventario {

	private Long id;

	private Date dataInventario;

	private Setor setor;

	private List<Tag> tags;

	private boolean registroAtivo;

	public Inventario() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventario other = (Inventario) obj;
		if (dataInventario == null) {
			if (other.dataInventario != null)
				return false;
		} else if (!dataInventario.equals(other.dataInventario))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (registroAtivo != other.registroAtivo)
			return false;
		if (setor == null) {
			if (other.setor != null)
				return false;
		} else if (!setor.equals(other.setor))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	@JsonProperty("dataInventario")
	@XmlElement(name = "dataInventario")
	public Date getDataInventario() {
		return dataInventario;
	}

	@JsonProperty("id")
	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	@JsonProperty("setor")
	@XmlElement(name = "setor")
	public Setor getSetor() {
		return setor;
	}

	@JsonProperty("tags")
	@XmlElement(name = "tags")
	public List<Tag> getTags() {
		if (tags == null) {
			tags = new ArrayList<Tag>();
		}
		return tags;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataInventario == null) ? 0 : dataInventario.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (registroAtivo ? 1231 : 1237);
		result = prime * result + ((setor == null) ? 0 : setor.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@JsonProperty("registroAtivo")
	@XmlElement(name = "registroAtivo")
	public boolean isRegistroAtivo() {
		return registroAtivo;
	}

	public void setDataInventario(Date dataInventario) {
		this.dataInventario = dataInventario;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRegistroAtivo(boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

}