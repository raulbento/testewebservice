import java.util.Date;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * CLiente WebService do RFLOG.
 * 
 * @author Raul Silveira Bento (bentoraul@gmail.com)
 *
 */
public class Teste {

	private static final String ENDERECO_ESTABELECIMENTO = "http://192.168.0.3:8080/rflog/service/estabelecimento/XML/salvar";
	
	private static final String ENDERECO_GRUPO_INVENTARIO = "http://192.168.0.3:8080/rflog/service/grupoinventario/XML/salvar";

	private static final String TYPE = "application/xml";

	public static void main(String[] args) {
		try {
			// ClientResponse response = testarWebServiceEstabelecimento();
			ClientResponse response = testarWebServiceGrupoInventar();
			validar(response);
			System.out.println("Resposta: ");
			System.out.println(response.getEntity(String.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static ClientResponse testarWebServiceEstabelecimento() {

		Setor setor = new Setor();
		setor.setId(2L);
		setor.setDescricao("TI");
		setor.setRegistroAtivo(true);

		Estabelecimento estabelecimento = new Estabelecimento();
		estabelecimento.setId(1L);
		estabelecimento.setDescricao("Danashe Aldeota");
		estabelecimento.getSetores().add(setor);
		estabelecimento.setCnpj("25.080.566/0001-00");
		estabelecimento.setValidade(true);
		estabelecimento.setRegistroAtivo(true);

		return webService(estabelecimento, ENDERECO_ESTABELECIMENTO);
	}

	private static ClientResponse testarWebServiceGrupoInventar() {

		Setor setor = new Setor();
		setor.setId(2L);
		setor.setDescricao("TI");
		setor.setRegistroAtivo(true);

		Estabelecimento estabelecimento = new Estabelecimento();
		estabelecimento.setId(1L);
		estabelecimento.setDescricao("Danashe Aldeota");
		estabelecimento.getSetores().add(setor);
		estabelecimento.setCnpj("25.080.566/0001-00");
		estabelecimento.setRegistroAtivo(true);

		Tag tag = new Tag();
		tag.setId(1L);
		tag.setEpc("234TESTE24434234234234");
		tag.setRegistroAtivo(true);

		Inventario inventario = new Inventario();
		inventario.setSetor(setor);
		inventario.setDataInventario(new Date());
		inventario.getTags().add(tag);
		inventario.setRegistroAtivo(true);

		GrupoInventario grupoInventario = new GrupoInventario();
		grupoInventario.setEstabelecimento(estabelecimento);
		grupoInventario.setCompleto(true);
		grupoInventario.setCriacao(new Date());
		grupoInventario.getInventarios().add(inventario);
		grupoInventario.setRegistroAtivo(true);

		return webService(grupoInventario, ENDERECO_GRUPO_INVENTARIO);
	}

	/*
	 * Executa operação.
	 */
	private static ClientResponse webService(Object object, String endereco) {
		WebResource webResource = Client.create().resource(endereco);
		return webResource.type(TYPE).post(ClientResponse.class, object);
	}

	private static void validar(ClientResponse response) throws RuntimeException {
		if (response.getStatus() != 200) {
			throw new RuntimeException(new StringBuilder().append("Falha : Erro de código de HTTP: ")
					.append(response.getStatus()).toString());
		}
	}

}
