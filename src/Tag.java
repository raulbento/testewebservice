import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;


@XmlRootElement(name = "tag")
public class Tag {

	private Long id;

	private String epc;

	private Setor setor;

	private boolean registroAtivo;

	public Tag() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (epc == null) {
			if (other.epc != null)
				return false;
		} else if (!epc.equals(other.epc))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (registroAtivo != other.registroAtivo)
			return false;
		if (setor == null) {
			if (other.setor != null)
				return false;
		} else if (!setor.equals(other.setor))
			return false;
		return true;
	}

	@JsonProperty("epc")
	@XmlElement(name = "epc")
	public String getEpc() {
		return epc;
	}

	@JsonProperty("id")
	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	@JsonProperty("setor")
	@XmlElement(name = "setor")
	public Setor getSetor() {
		if (setor == null) {
			setor = new Setor();
		}
		return setor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((epc == null) ? 0 : epc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (registroAtivo ? 1231 : 1237);
		result = prime * result + ((setor == null) ? 0 : setor.hashCode());
		return result;
	}

	@JsonProperty("registroAtivo")
	@XmlElement(name = "registroAtivo")
	public boolean isRegistroAtivo() {
		return registroAtivo;
	}

	public void setEpc(String epc) {
		this.epc = epc;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRegistroAtivo(boolean isRegistroAtivo) {
		this.registroAtivo = isRegistroAtivo;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

}
