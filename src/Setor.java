import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement(name = "setor")
public class Setor {

	private Long id;

	private String descricao;

	private boolean registroAtivo;

	public Setor() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Setor other = (Setor) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (registroAtivo != other.registroAtivo)
			return false;
		return true;
	}

	@JsonProperty("descricao")
	@XmlElement(name = "descricao")
	public String getDescricao() {
		return descricao;
	}

	@JsonProperty("id")
	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (registroAtivo ? 1231 : 1237);
		return result;
	}

	@JsonProperty("isRegistroAtivo")
	@XmlElement(name = "isRegistroAtivo")
	public boolean isRegistroAtivo() {
		return registroAtivo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRegistroAtivo(boolean isRegistroAtivo) {
		this.registroAtivo = isRegistroAtivo;
	}

}
