import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "estabelecimento")
public class Estabelecimento {

	private Long id;

	private String descricao;

	private boolean validade;

	private String cnpj;

	private List<Setor> setores;

	private boolean registroAtivo;

	@XmlElement(name = "cnpj")
	public String getCnpj() {
		return cnpj;
	}

	@XmlElement(name = "descricao")
	public String getDescricao() {
		return descricao;
	}

	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	@XmlElement(name = "setores")
	public List<Setor> getSetores() {
		if (setores == null) {
			setores = new ArrayList<Setor>();
		}
		return setores;
	}

	@XmlElement(name = "registroAtivo")
	public boolean isRegistroAtivo() {
		return registroAtivo;
	}

	@XmlElement(name = "validade")
	public boolean isValidade() {
		return validade;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRegistroAtivo(boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public void setValidade(boolean validade) {
		this.validade = validade;
	}

}
